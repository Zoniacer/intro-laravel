<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="{{ route('welcome') }}" method="POST">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname" required><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname" required><br>

        <p>Gender:</p>
        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="Indonesia" id="Indonesia" value="Indonesia">
        <label for="Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="English" id="English" value="English">
        <label for="English">English</label><br>
        <input type="checkbox" name="Other_lang" id="Other_lang" value="Other_lang">
        <label for="Other_lang">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>
